#include <gtest\gtest.h>
#include "ttext.h"
#include <locale>

#include <iostream>

TEST(TText, can_create_text)
{
    ASSERT_NO_THROW(TText text);
}

TEST(TText, can_create_not_empty_text)
{
    TTextLink pl("string");
    TTextLink::InitMemSystem(1);

    ASSERT_NO_THROW(TText text(&pl));
}

TEST(TText, can_go_first_link)
{
    TText text;
    ASSERT_NO_THROW(text.GoFirstLink());
}

TEST(TText, return_error_when_GoFirstLink_in_empty_text)
{
    TText text;
    ASSERT_EQ(text.GoFirstLink(), DataErr);
}

TEST(TText, can_GoDownLink)
{
    const string str1 = "str1", str2 = "str2";
    TTextLink::InitMemSystem(2);
    TText text;

    text.GoFirstLink();
    text.SetLine(str1);
    text.InsDownLine(str2);

    ASSERT_NO_THROW(text.GoDownLink());
}

TEST(TText, GoDownLink_go_down)
{
    const string str1 = "str1", str2 = "str2";
    TTextLink::InitMemSystem(2);
    TText text;

    text.GoFirstLink();
    text.SetLine(str1);
    text.InsDownLine(str2);
    text.GoDownLink();

    ASSERT_EQ(text.GetLine(), str2);
}

TEST(TText, can_GoNextLink)
{
    const string str1 = "str1", str2 = "str2";
    TTextLink::InitMemSystem(2);
    TText text;

    text.GoFirstLink();
    text.SetLine(str1);
    text.InsNextLine(str2);

    ASSERT_NO_THROW(text.GoNextLink());
}

TEST(TText, GoNextLink_go_next)
{
    const string str1 = "str1", str2 = "str2";
    TTextLink::InitMemSystem(2);
    TText text;

    text.GoFirstLink();
    text.SetLine(str1);
    text.InsNextLine(str2);
    text.GoNextLink();

    ASSERT_EQ(text.GetLine(), str2);
}

TEST(TText, can_GoPrevLink)
{
    const string str1 = "str1", str2 = "str2";
    TTextLink::InitMemSystem(2);
    TText text;

    text.GoFirstLink();
    text.SetLine(str1);
    text.InsNextLine(str2);

    ASSERT_NO_THROW(text.GoPrevLink());
}

TEST(TText, GoPrevLink_go_prev)
{
    const string str1 = "str1", str2 = "str2";
    TTextLink::InitMemSystem(2);
    TText text;

    text.GoFirstLink();
    text.SetLine(str1);
    text.InsNextLine(str2);
    text.GoNextLink();
    text.GoPrevLink();

    ASSERT_EQ(text.GetLine(), str1);
}

TEST(TText, can_set_line)
{
    const string str = "str";
    TTextLink::InitMemSystem(1);
    TText text;

    text.GoFirstLink();

    ASSERT_NO_THROW(text.SetLine(str));
}

TEST(TText, set_line_eq_to_get_line)
{
    const string str = "str";
    TTextLink::InitMemSystem(1);
    TText text;

    text.GoFirstLink();
    text.SetLine(str);

    ASSERT_EQ(text.GetLine(), str);
}

TEST(TText, can_ins_down_line)
{
    const string str1 = "str1", str2 = "str2";
    TTextLink::InitMemSystem(2);
    TText text;

    text.GoFirstLink();
    text.SetLine(str1);

    ASSERT_NO_THROW(text.InsDownLine(str2));
}

TEST(TText, can_ins_down_section)
{
    const string str1 = "str1", str2 = "str2";
    TTextLink::InitMemSystem(2);
    TText text;

    text.GoFirstLink();
    text.SetLine(str1);

    ASSERT_NO_THROW(text.InsDownSection(str2));
}

TEST(TText, can_ins_next_line)
{
    const string str1 = "str1", str2 = "str2";
    TTextLink::InitMemSystem(2);
    TText text;

    text.GoFirstLink();
    text.SetLine(str1);

    ASSERT_NO_THROW(text.InsNextLine(str2));
}

TEST(TText, can_ins_next_section)
{
    const string str1 = "str1", str2 = "str2";
    TTextLink::InitMemSystem(2);
    TText text;

    text.GoFirstLink();
    text.SetLine(str1);

    ASSERT_NO_THROW(text.InsNextSection(str2));
}

TEST(TText, can_del_down_line)
{
    const string str1 = "str1", str2 = "str2";
    TTextLink::InitMemSystem(2);
    TText text;

    text.GoFirstLink();
    text.SetLine(str1);
    text.InsDownLine(str2);

    ASSERT_NO_THROW(text.DelDownLine());
}

TEST(TText, can_del_down_section)
{
    const string str1 = "str1", str2 = "str2";
    TTextLink::InitMemSystem(2);
    TText text;

    text.GoFirstLink();
    text.SetLine(str1);
    text.InsDownSection(str2);

    ASSERT_NO_THROW(text.DelDownSection());
}

TEST(TText, can_del_next_line)
{
    const string str1 = "str1", str2 = "str2";
    TTextLink::InitMemSystem(2);
    TText text;

    text.GoFirstLink();
    text.SetLine(str1);
    text.InsNextLine(str2);

    ASSERT_NO_THROW(text.DelNextLine());
}

TEST(TText, can_del_next_section)
{
    const string str1 = "str1", str2 = "str2";
    TTextLink::InitMemSystem(2);
    TText text;

    text.GoFirstLink();
    text.SetLine(str1);
    text.InsNextSection(str2);

    ASSERT_NO_THROW(text.DelNextSection());
}
