#include "ttext.h"

int main() {
    setlocale(LC_ALL, "Russian");

    PTText pText;
    TTextLink::InitMemSystem(50);
    TText text;

    text.Read("input.txt");
    cout << "�����, ����������� �� ����� input.txt:" << endl << endl;
    text.Print();
    cout << endl;

    cout << "���������������� �����:" << endl << endl;
    pText = text.getCopy();
    pText->GoFirstLink();
    pText->GoDownLink();
    pText->SetLine("����");
    pText->GoDownLink();
    pText->GoDownLink();
    pText->InsDownLine("����� 1");
    pText->GoNextLink();
    pText->DelNextLine();
    pText->GoPrevLink();
    pText->GoPrevLink();
    pText->GoPrevLink();
    pText->GoNextLink();
    pText->SetLine("���������� ����");
    pText->GoDownLink();
    pText->GoNextLink();
    pText->GoDownLink();
    pText->DelDownSection();
    pText->GoPrevLink();
    pText->GoPrevLink();
    pText->GoPrevLink();
    pText->GoNextLink();
    pText->SetLine("��");
    pText->Print();

    pText->Write("output.txt");
    cout << endl << "���������������� ����� �������� � ���� output.txt" << endl;
    return 0;
}