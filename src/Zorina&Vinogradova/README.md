## Методы программирования 2: Тексты

### Цели и задачи

В рамках лабораторной работы ставится задача разработки учебного редактора текстов с поддержкой следующих операций:

- выбор текста для редактирования (или создание нового текста);
- демонстрация текста на экране дисплея;
- поддержка средств указания элементов (уровней) текста;
- вставка, удаление и замена строк текста;
- запись подготовленного текста в файл.

При выполнении операций чтения (при выборе для редактирования уже существующего текста) и записи редактор должен использовать стандартный формат, принятый в файловой системе для представления текстовых файлов, обеспечивая тем самым совместимость учебного редактора текстов и существующего программного обеспечения.

#### Условия и ограничения

В рамках выполнения данной лабораторной работы могут быть использованы следующие основные допущения:

1. При планировании структуры текста в качестве самого нижнего уровня можно рассматривать уровень строк.
2. В качестве тестовых текстов можно рассматривать текстовые файлы программы.

#### План работы

1. Разработка структуры хранения текстов.
2. Разработка методов для доступа к строкам текста.
3. Возможность модификации структуры текста.
4. Создание механизма для обхода текста.
5. Реализация итератора.
6. Копирование текста.
7. Реализация сборки мусора.
8. Тестирование.

### Выполнение работы

#### TTextLink
TTextLink.h
```cpp
#ifndef __TEXTLINK_H__
#define __TEXTLINK_H__

#include <iostream>

#include "tdatvalue.h"

using namespace std;

#define MemSize 25
#define TextLineLength 225

class TTextLink;
typedef TTextLink *PTTextLink;
typedef char TStr[TextLineLength];

class TTextMem 
{
	PTTextLink pFirst;     // указатель на первое звено
	PTTextLink pLast;      // указатель на последнее звено
	PTTextLink pFree;      // указатель на первое свободное звено
    TTextMem() : pFirst(nullptr), pLast(nullptr), pFree(nullptr) {}
	friend class TTextLink;
};

typedef TTextMem *PTTextMem;
class TText;

class TTextLink : public TDatValue
{
protected:
	TStr Str;                       // поле для хранения строки текста
	PTTextLink pNext, pDown;        // указатели по тек. уровень и на подуровень
	static TTextMem MemHeader;      // система управления памятью
public:
	static void InitMemSystem(int size = MemSize); // инициализация памяти
	static void PrintFreeLink(void);               // печать свободных звеньев
	void * operator new (size_t size);             // выделение звена
	void operator delete (void *pM);               // освобождение звена
	static void MemCleaner(TText& txt);            // сборка мусора
	TTextLink(TStr s = NULL, PTTextLink pn = NULL, PTTextLink pd = NULL)
	{
		pNext = pn;
		pDown = pd;
		if (s != NULL)
			strcpy_s(Str, s); 
		else 
			Str[0] = '\0';
	}
    TTextLink(string s)
    {
        pNext = nullptr;
        pDown = nullptr;
        strcpy_s(Str, s.c_str());
    }
	~TTextLink() {}
	bool IsAtom() { return pDown == nullptr; } // проверка атомарности звена
	PTTextLink GetNext() { return pNext; }
	PTTextLink GetDown() { return pDown; }
	PTDatValue GetCopy() { return new TTextLink(Str, pNext, pDown); }
protected:
    virtual void Print(ostream &os) { os << Str; }
	friend class TText;
};

#endif
```
TTextLink.cpp
```cpp
#include "ttextlink.h"
#include "ttext.h"

TTextMem TTextLink::MemHeader;

void TTextLink::InitMemSystem(int size) // инициализация памяти
{
    char *tmp = new char[sizeof(TTextLink)*size];
    MemHeader.pFirst = (PTTextLink)tmp;
    MemHeader.pFree = (PTTextLink)tmp;
    MemHeader.pLast = (PTTextLink)tmp + size - 1;
    PTTextLink pLink = MemHeader.pFirst;
    for (int i = 0; i < size - 1; i++, pLink++)
        pLink->pNext = pLink + 1;
    pLink->pNext = nullptr;
}

void TTextLink::PrintFreeLink()  // печать свободных звеньев
{
    PTTextLink pLink = MemHeader.pFree;
    while (pLink != nullptr)
    {
        cout << pLink->Str << endl;
        pLink = pLink->pNext;
    }
}

void * TTextLink::operator new (size_t size) // выделение звена
{
	PTTextLink pLink = MemHeader.pFree;
	if (MemHeader.pFree != nullptr)
		MemHeader.pFree = pLink->pNext;
	return pLink;
}

void TTextLink::operator delete (void *pM)   // освобождение звена
{
    PTTextLink pLink = (PTTextLink)pM;
    pLink->pNext = MemHeader.pFree;
    MemHeader.pFree = pLink;
}

void TTextLink::MemCleaner(TText &txt) // сборка мусора
{
	for (txt.Reset(); !txt.IsTextEnded(); txt.GoNext())
		txt.SetLine("&&&" + txt.GetLine());
    PTTextLink pLink = MemHeader.pFree;
    for (; pLink != nullptr; pLink = pLink->pNext)
        strcpy_s(pLink->Str, "&&&");
    for (pLink = MemHeader.pFirst; pLink <= MemHeader.pLast; pLink++)
    {
        if (strstr(pLink->Str, "&&&") != nullptr)
            strcpy_s(pLink->Str, pLink->Str + 3);
        else
            delete pLink;
    }
}
```

#### TText
TText.h
```cpp
#ifndef __TEXT_H__
#define __TEXT_H__

#include <iostream>
#include <stack>
#include <string>
#include <fstream>
#include "tdatacom.h"
#include "ttextlink.h"

using namespace std;

class TText;
typedef TText *PTText;

class TText : public TDataCom
{
protected:
	PTTextLink pFirst;                                       // указатель корня дерева
	PTTextLink pCurrent;                                     // указатель текущей строки
	stack< PTTextLink > Path;                                // стек траектории движения по тексту
	stack< PTTextLink > St;                                  // стек для итератора
    PTTextLink GetFirstAtom(PTTextLink pl);                  // поиск первого атома
	void PrintText(PTTextLink ptl);                          // печать текста со звена ptl
    void PrintTextInFile(PTTextLink ptl, ofstream &TxtFile); // печать текста в файл со звена ptl
	PTTextLink ReadText(ifstream &TxtFile);                  // чтение текста из файла
    static int TextLevel;
public:
	TText(PTTextLink pl = nullptr);
	~TText() { pFirst = nullptr; }
	PTText getCopy();
	// навигация
	int GoFirstLink(void); // переход к первой строке
	int GoDownLink(void);  // переход к следующей строке по Down
	int GoNextLink(void);  // переход к следующей строке по Next
	int GoPrevLink(void);  // переход к предыдущей позиции в тексте
	// доступ
	string GetLine(void);   // чтение текущей строки
	void SetLine(string s); // замена текущей строки 
	// модификация
	void InsDownLine(string s);    // вставка строки в подуровень
	void InsDownSection(string s); // вставка раздела в подуровень
	void InsNextLine(string s);    // вставка строки в том же уровне
	void InsNextSection(string s); // вставка раздела в том же уровне
	void DelDownLine(void);        // удаление строки в подуровне
	void DelDownSection(void);     // удаление раздела в подуровне
	void DelNextLine(void);        // удаление строки в том же уровне
	void DelNextSection(void);     // удаление раздела в том же уровне
	// итератор
	int Reset();               // установить на первую звапись
	bool IsTextEnded() const;   // текст завершен?
	int GoNext(void);              // переход к следующей записи
	//работа с файлами
	void Read(char * pFileName);   // ввод текста из файла
	void Write(char * pFileName);  // вывод текста в файл
	//печать
	void Print(void);              // печать текста
};

#endif
```
TText.cpp
```cpp
#include "ttext.h"

int TText::TextLevel;

PTTextLink TText::GetFirstAtom(PTTextLink pl) // поиск первого атома
{
    PTTextLink tmp = pl;
    while (!tmp->IsAtom())
    {
        St.push(tmp);
        tmp = tmp->GetDown();
    }
    return tmp;
}

void TText::PrintText(PTTextLink ptl) // печать текста со звена ptl
{
    if (ptl != nullptr)
    {
        for (int i = 0; i < TextLevel; i++)
            cout << "  ";
        cout << ptl->Str << endl;
        TextLevel++;
        PrintText(ptl->pDown);
        TextLevel--;
        PrintText(ptl->GetNext());
    }
}

void TText::PrintTextInFile(PTTextLink ptl, ofstream &TxtFile) // печать текста в файл со звена ptl
{
    if (ptl != nullptr)
    {
        for (int i = 0; i < TextLevel; i++)
            TxtFile << "  ";
        TxtFile << ptl->Str << endl;
        TextLevel++;
        PrintTextInFile(ptl->GetDown(), TxtFile);
        TextLevel--;
        PrintTextInFile(ptl->GetNext(), TxtFile);
    }
}

PTTextLink TText::ReadText(ifstream &TxtFile) //чтение текста из файла
{
    string buf;
    PTTextLink ptl = new TTextLink();
    PTTextLink tmp = ptl;
    while (!TxtFile.eof())
    {
        getline(TxtFile, buf);
        if (buf.front() == '}')
            break;
        else if (buf.front() == '{')
            ptl->pDown = ReadText(TxtFile);
        else
        {
            ptl->pNext = new TTextLink(buf.c_str());
            ptl = ptl->pNext;
        }
    }
    ptl = tmp;
    if (tmp->pDown == nullptr)
    {
        tmp = tmp->pNext;
        delete ptl;
    }
    return tmp;
}


TText::TText(PTTextLink pl)
{
    if (pl == nullptr)
        pl = new TTextLink();
    pFirst = pl;
}

PTText TText::getCopy()
{
    PTTextLink pl1, pl2, pl = pFirst, copied = nullptr;
    bool flag = true;
    
    if (pFirst != nullptr)
    {
        while (!St.empty())
            St.pop();
        while (flag)
        {
            if (pl != nullptr)
            {
                pl = GetFirstAtom(pl);
                St.push(pl);
                pl = pl->GetDown();
            }
            else if (St.empty())
                flag = false;
            else
            {
                pl1 = St.top();
                St.pop();
                if (strstr(pl1->Str, "Copy") == nullptr)
                {
                    pl2 = new TTextLink("Copy", pl1, copied);
                    St.push(pl2);
                    pl = pl1->GetNext();
                    copied = nullptr;
                }
                else
                {
                    pl2 = pl1->GetNext();
                    strcpy_s(pl1->Str, pl2->Str);
                    pl1->pNext = copied;
                    copied = pl1;
                }
            }
        }

    }
    return new TText(copied);
}

// навигация
int TText::GoFirstLink() // переход к первой строке
{
    while (!Path.empty())
        Path.pop();
    pCurrent = pFirst;
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    return RetCode;
}

int TText::GoDownLink()  // переход к следующей строке по Down
{
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    else if (pCurrent->pDown == nullptr)
        SetRetCode(DataErr);
    else
    {
        Path.push(pCurrent);
        pCurrent = pCurrent->GetDown();

    }
    return RetCode;
}

int TText::GoNextLink()  // переход к следующей строке по Next
{
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    else if (pCurrent->pNext == nullptr)
        SetRetCode(DataErr);
    else
    {
        Path.push(pCurrent);
        pCurrent = pCurrent->GetNext();
    }
    return RetCode;
}

int TText::GoPrevLink()  // переход к предыдущей позиции в тексте
{
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    else if (pCurrent == pFirst)
        SetRetCode(DataErr);
    else
    {
        pCurrent = Path.top();
        Path.pop();
    }
    return RetCode;
}

// доступ
string TText::GetLine()   // чтение текущей строки
{
    if (pCurrent == nullptr)
    {
        SetRetCode(DataErr);
        return "";
    }
    else
        return string(pCurrent->Str);
}

void TText::SetLine(string s) // замена текущей строки 
{
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    else
        strcpy_s(pCurrent->Str, s.c_str());
}

// модификация
void TText::InsDownLine(string s)    // вставка строки в подуровень
{
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    else
    {
        TStr buf;
        strcpy_s(buf, s.c_str());
        pCurrent->pDown = new TTextLink(buf, pCurrent->pDown, nullptr);
    }
}

void TText::InsDownSection(string s) // вставка раздела в подуровень
{
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    else
    {
        TStr buf;
        strcpy_s(buf, s.c_str());
        pCurrent->pDown = new TTextLink(buf, nullptr, pCurrent->pDown);
    }
}

void TText::InsNextLine(string s)    // вставка строки в том же уровне
{
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    else
    {
        TStr buf;
        strcpy_s(buf, s.c_str());
        pCurrent->pNext = new TTextLink(buf, pCurrent->pNext, nullptr);
    }
}

void TText::InsNextSection(string s) // вставка раздела в том же уровне
{
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    else
    {
        TStr buf;
        strcpy_s(buf, s.c_str());
        pCurrent->pNext = new TTextLink(buf, nullptr, pCurrent->pNext);
    }
}

void TText::DelDownLine()        // удаление строки в подуровне
{
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    else if (pCurrent->pDown == nullptr)
        SetRetCode(DataErr);
    else if (pCurrent->pDown->IsAtom())
        pCurrent->pDown = pCurrent->pDown->pNext;
}

void TText::DelDownSection()     // удаление раздела в подуровне
{
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    else if (pCurrent->pDown == nullptr)
        SetRetCode(DataErr);
    else
        pCurrent->pDown = nullptr;
}

void TText::DelNextLine()        // удаление строки в том же уровне
{
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    else if (pCurrent->pNext == nullptr)
        SetRetCode(DataErr);
    else if (pCurrent->pNext->IsAtom())
        pCurrent->pNext = pCurrent->pNext->pNext;
}

void TText::DelNextSection()     // удаление раздела в том же уровне
{
    if (pCurrent == nullptr)
        SetRetCode(DataErr);
    else if (pCurrent->pNext == nullptr)
        SetRetCode(DataErr);
    else
        pCurrent->pNext = pCurrent->pNext->pNext;
}

// итератор
int TText::Reset()              // установить на первую звапись
{
    GoFirstLink();
    if (pCurrent != nullptr)
    {
        St.push(pCurrent);
        if (pCurrent->pNext != nullptr)
            St.push(pCurrent->pNext);
        if (pCurrent->pDown != nullptr)
            St.push(pCurrent->pDown);
    }
    return RetCode;
}

bool TText::IsTextEnded() const  // текст завершен?
{
    return St.empty();
}

int TText::GoNext()             // переход к следующей записи
{
    if (!IsTextEnded())
    {
        pCurrent = St.top();
        St.pop();
        if (pCurrent != pFirst)
        {
            if (pCurrent->pNext != nullptr)
                St.push(pCurrent->pNext);
            if (pCurrent->pDown != nullptr)
                St.push(pCurrent->pDown);
        }

    }
    return IsTextEnded();
}

//работа с файлами
void TText::Read(char * pFileName)  // ввод текста из файла
{
    ifstream TextFile(pFileName);
    TextLevel = 0;
    if (TextFile)
        pFirst = ReadText(TextFile);
}

void TText::Write(char * pFileName) // вывод текста в файл
{
    ofstream TextFile(pFileName);
    TextLevel = 0;
    if (TextFile)
        PrintTextInFile(pFirst, TextFile);
}

//печать
void TText::Print()             // печать текста
{
    TextLevel = 0;
    PrintText(pFirst);
}
```

#### Тесты для класса TText
```cpp
#include <gtest\gtest.h>
#include "ttext.h"
#include <locale>

#include <iostream>

TEST(TText, can_create_text)
{
    ASSERT_NO_THROW(TText text);
}

TEST(TText, can_create_not_empty_text)
{
    TTextLink pl("string");
    TTextLink::InitMemSystem(1);

    ASSERT_NO_THROW(TText text(&pl));
}

TEST(TText, can_go_first_link)
{
    TText text;
    ASSERT_NO_THROW(text.GoFirstLink());
}

TEST(TText, return_error_when_GoFirstLink_in_empty_text)
{
    TText text;
    ASSERT_EQ(text.GoFirstLink(), DataErr);
}

TEST(TText, can_GoDownLink)
{
    const string str1 = "str1", str2 = "str2";
    TTextLink::InitMemSystem(2);
    TText text;

    text.GoFirstLink();
    text.SetLine(str1);
    text.InsDownLine(str2);

    ASSERT_NO_THROW(text.GoDownLink());
}

TEST(TText, GoDownLink_go_down)
{
    const string str1 = "str1", str2 = "str2";
    TTextLink::InitMemSystem(2);
    TText text;

    text.GoFirstLink();
    text.SetLine(str1);
    text.InsDownLine(str2);
    text.GoDownLink();

    ASSERT_EQ(text.GetLine(), str2);
}

TEST(TText, can_GoNextLink)
{
    const string str1 = "str1", str2 = "str2";
    TTextLink::InitMemSystem(2);
    TText text;

    text.GoFirstLink();
    text.SetLine(str1);
    text.InsNextLine(str2);

    ASSERT_NO_THROW(text.GoNextLink());
}

TEST(TText, GoNextLink_go_next)
{
    const string str1 = "str1", str2 = "str2";
    TTextLink::InitMemSystem(2);
    TText text;

    text.GoFirstLink();
    text.SetLine(str1);
    text.InsNextLine(str2);
    text.GoNextLink();

    ASSERT_EQ(text.GetLine(), str2);
}

TEST(TText, can_GoPrevLink)
{
    const string str1 = "str1", str2 = "str2";
    TTextLink::InitMemSystem(2);
    TText text;

    text.GoFirstLink();
    text.SetLine(str1);
    text.InsNextLine(str2);

    ASSERT_NO_THROW(text.GoPrevLink());
}

TEST(TText, GoPrevLink_go_prev)
{
    const string str1 = "str1", str2 = "str2";
    TTextLink::InitMemSystem(2);
    TText text;

    text.GoFirstLink();
    text.SetLine(str1);
    text.InsNextLine(str2);
    text.GoNextLink();
    text.GoPrevLink();

    ASSERT_EQ(text.GetLine(), str1);
}

TEST(TText, can_set_line)
{
    const string str = "str";
    TTextLink::InitMemSystem(1);
    TText text;

    text.GoFirstLink();

    ASSERT_NO_THROW(text.SetLine(str));
}

TEST(TText, set_line_eq_to_get_line)
{
    const string str = "str";
    TTextLink::InitMemSystem(1);
    TText text;

    text.GoFirstLink();
    text.SetLine(str);

    ASSERT_EQ(text.GetLine(), str);
}

TEST(TText, can_ins_down_line)
{
    const string str1 = "str1", str2 = "str2";
    TTextLink::InitMemSystem(2);
    TText text;

    text.GoFirstLink();
    text.SetLine(str1);

    ASSERT_NO_THROW(text.InsDownLine(str2));
}

TEST(TText, can_ins_down_section)
{
    const string str1 = "str1", str2 = "str2";
    TTextLink::InitMemSystem(2);
    TText text;

    text.GoFirstLink();
    text.SetLine(str1);

    ASSERT_NO_THROW(text.InsDownSection(str2));
}

TEST(TText, can_ins_next_line)
{
    const string str1 = "str1", str2 = "str2";
    TTextLink::InitMemSystem(2);
    TText text;

    text.GoFirstLink();
    text.SetLine(str1);

    ASSERT_NO_THROW(text.InsNextLine(str2));
}

TEST(TText, can_ins_next_section)
{
    const string str1 = "str1", str2 = "str2";
    TTextLink::InitMemSystem(2);
    TText text;

    text.GoFirstLink();
    text.SetLine(str1);

    ASSERT_NO_THROW(text.InsNextSection(str2));
}

TEST(TText, can_del_down_line)
{
    const string str1 = "str1", str2 = "str2";
    TTextLink::InitMemSystem(2);
    TText text;

    text.GoFirstLink();
    text.SetLine(str1);
    text.InsDownLine(str2);

    ASSERT_NO_THROW(text.DelDownLine());
}

TEST(TText, can_del_down_section)
{
    const string str1 = "str1", str2 = "str2";
    TTextLink::InitMemSystem(2);
    TText text;

    text.GoFirstLink();
    text.SetLine(str1);
    text.InsDownSection(str2);

    ASSERT_NO_THROW(text.DelDownSection());
}

TEST(TText, can_del_next_line)
{
    const string str1 = "str1", str2 = "str2";
    TTextLink::InitMemSystem(2);
    TText text;

    text.GoFirstLink();
    text.SetLine(str1);
    text.InsNextLine(str2);

    ASSERT_NO_THROW(text.DelNextLine());
}

TEST(TText, can_del_next_section)
{
    const string str1 = "str1", str2 = "str2";
    TTextLink::InitMemSystem(2);
    TText text;

    text.GoFirstLink();
    text.SetLine(str1);
    text.InsNextSection(str2);

    ASSERT_NO_THROW(text.DelNextSection());
}
```
![](./test_ttext.png)

#### Демонстрационная программа
```cpp
#include "ttext.h"

int main() {
    setlocale(LC_ALL, "Russian");

    PTText pText;
    TTextLink::InitMemSystem(50);
    TText text;

    text.Read("input.txt");
    cout << "Текст, прочитанный из файла input.txt:" << endl << endl;
    text.Print();
    cout << endl;

    cout << "Модифицированный текст:" << endl << endl;
    pText = text.getCopy();
    pText->GoFirstLink();
    pText->GoDownLink();
    pText->SetLine("АиСД");
    pText->GoDownLink();
    pText->GoDownLink();
    pText->InsDownLine("Пункт 1");
    pText->GoNextLink();
    pText->DelNextLine();
    pText->GoPrevLink();
    pText->GoPrevLink();
    pText->GoPrevLink();
    pText->GoNextLink();
    pText->SetLine("Английский язык");
    pText->GoDownLink();
    pText->GoNextLink();
    pText->GoDownLink();
    pText->DelDownSection();
    pText->GoPrevLink();
    pText->GoPrevLink();
    pText->GoPrevLink();
    pText->GoNextLink();
    pText->SetLine("ОС");
    pText->Print();

    pText->Write("output.txt");
    cout << endl << "Модифицированный текст сохранен в файл output.txt" << endl;
    return 0;
}
```
![](./main.png)

### Вывод
В ходе лабораторной работы была разработана структура данных для хранения и редактирования текста, а именно навигация по тексту, доступ и модификация его элементов и т. д.